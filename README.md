# README #

### What is this repository for? ###

This is a VERY SIMPLE example to show the use of Alignment API (version 4.7) to perform the mapping of CSV columns/data to ontology concepts. This example was generating to answer a [question](http://answers.semanticweb.com/questions/31641/use-an-ontology-to-store-csv-data-into-rdf-form) about mapping approach.

### How do I get set up? ###

This example was built using Eclipse Kepler, and the repository contains the whole eclipse project.  Apache Jena is set as a maven dependency, and the Alignment API was imported as a Eclipse User Library. The Alignment API can be found [here](http://alignapi.gforge.inria.fr/), where you can find instructions to build a maven repo too.

### Who do I talk to? ###

* This example was built by Rodrigo C. Antonialli (rcantonialli@gmail.com)