package br.rcantonialli.example;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.AlignmentProcess;
import org.semanticweb.owl.align.AlignmentVisitor;

import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.ModelFactory;

import fr.inrialpes.exmo.align.impl.method.SMOANameAlignment;
import fr.inrialpes.exmo.align.impl.renderer.RDFRendererVisitor;
import fr.inrialpes.exmo.ontowrap.OntowrapException;
import fr.inrialpes.exmo.ontowrap.jena25.JENAOntology;
import fr.inrialpes.exmo.ontowrap.jena25.JENAOntologyFactory;

/**
 * This is a sample class to perform a mapping from a CSV file to an ontology.
 *
 * This approach considers to transform the CSV into a RDF model (simple ontolgy actually)
 * and then align it with a reference ontology (a simple one create for tests purpose).
 *
 * The Alignment API library can be found http://alignapi.gforge.inria.fr/
 * There are instructions to create a local maven repo, but for this test, it was imported as a Eclipse Library.
 *
 * @author Rodrigo C. Antonialli
 * 16/02/2015 - 09:23:44
 *
 */
public class CSVtoRDFMapping {

	/**
	 * Default constructor
	 */
	public CSVtoRDFMapping() {}

	/**
	 * @param args
	 */
	public void performMapping() {

		int linecount = 0;

		//CSV column delimiter
		String delimiter = ",";

		//Reading CSV data file
		File csvFile = new File("dataSource/TempSensor1.csv");

		//This will be a class name in the CSV - RDF model.
		String csvClassName = csvFile.getName();
		csvClassName = csvClassName.substring(0, csvClassName.lastIndexOf("."));

		//The model to hold the CSV RDF serialization. This could be a generic RDF model too.
		OntModel csvModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);

		String csvURI = "http://example.com/csv/";
		csvModel.createOntology(csvURI.substring(0, csvURI.lastIndexOf("/")));
		csvModel.getNsPrefixMap().put("csv", csvURI );

		/*
		 * NOTE: In this example, I consider the file name as the table name (and CSV is kind of a table).
		 * Once a table is a set of things, it could be mapped as a class.
		 */
		OntClass cvsClass = csvModel.createClass(csvURI.concat(csvClassName));

		cvsClass.addLabel(csvClassName, "en");


		//Reading and parsing CSV file to write it as a RDF model
		try {

			BufferedReader breader = new BufferedReader(new FileReader(csvFile));

			String strRead = breader.readLine();

			//Just to keep the order and feel the right properties when creating individuals.
			List<String> colListing = new ArrayList<String>();

			while(strRead != null){
				StringTokenizer tokenizer = new StringTokenizer(strRead, delimiter);
				linecount++;

				if(linecount == 1){ //Reading header with col names. This should originate the "TBox", with schema concepts.
					while(tokenizer.hasMoreTokens()){

						String colname = tokenizer.nextToken();

						String colURI = csvURI.concat(colname.trim().toLowerCase().replace(" ", "_"));

						colListing.add(colURI);

						//Creating a DatatypeProperty from the column name...
						DatatypeProperty colDataProp = csvModel.createDatatypeProperty(colURI);
						colDataProp.addLabel(colname, "en");

					}
				}else{ //Reading content

					//Each line is a new individual

					Individual csvIndividual = cvsClass.createIndividual(csvURI.concat("individual".concat(String.valueOf(linecount))));

					int i = 0;

					while(tokenizer.hasMoreTokens()){
						String colvalue = tokenizer.nextToken();

						String property = colListing.get(i++);

						//You should do something to find the correct datatype for the property value.
						csvIndividual.addProperty(csvModel.getDatatypeProperty(property), colvalue);

					}
				}

				strRead = breader.readLine();
			}

			breader.close();

			/**
			 * Loading the reference ontology
			 */

			//Creating a Jena OntModel in memory
			OntModel sensorOntology = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);

			//Reading the ontology file into jena model
			sensorOntology.read("referenceOntology/sensorOntology.ttl");

			//Listing only datatype properties - for example purpose only. You should list all entities.
			//ExtendedIterator<DatatypeProperty> datatypeProperties = sensorOntology.listDatatypeProperties();

			/**
			 * Now, we have the reference ontology model (SSN, or in this case the simple version of it)
			 * and the CSV written as a simple rdf/ontology. It's time to find the matches. To do so, we'll use
			 * the Alignment API: http://alignapi.gforge.inria.fr/
			 *
			 * The Alignment API uses it's on syntax and models to represent ontologies, but it gives us some
			 * wrapping tools to use other API's, like OWL API and Jena.
			 *
			 */

			//The SMOANameAlignment is a sample from Alignment API that uses the smoa algorithm to compare string distances.
			AlignmentProcess alignProcess = new SMOANameAlignment();

			//The source ontology will be the one generated by the CSV:
			JENAOntology sourceOntology = new JENAOntologyFactory().newOntology(csvModel, true);

			//The target ontology will be the reference ontology:
			JENAOntology targetOntology = new JENAOntologyFactory().newOntology(sensorOntology, true);

			//Obs: The boolean parameter above defines 'onlyLocalEntities', i.e. no import closures are made.

			//Setting up the alignment process:
			alignProcess.init(sourceOntology, targetOntology);

			//The first param is a previous reference alignment that could be used to generate the new one.
			//The seconf param is used to set configuration parameters for the process.
			alignProcess.align(null, new Properties());

			/**
			 * Now, we have our alignment...
			 *
			 * From this alignment, you could do several things...
			 *
			 * You could get your individuals from CSV Model and create them in the reference ontology using
			 * the SSN vocabulary. You could also keep the CSV model stored and translate sparql queries.
			 * If I'm not mistaken, the Alignment API gives you tools to translate SPARQL.
			 *
			 * I'd recommend to take a look at Alignment Server, implemented using Alignment API (same website).
			 * It's easy to set it up and get it running and it gives plenty of tools to access via SOAP/REST.
			 *
			 * At last, you could extend some classes from Alignment API and build your own alignment method.
			 * I've been doing this for my Master Degree Thesis to align geospatial ontologies.
			 *
			 * For this example, we'll just print the alignment. Considering the sample reference ontology I created
			 * and the CSV example, the method got almost everything right, except for tmpf and timezone for example.
			 * You could correct this manually (or implement a better method to get the right answer). Once you do this,
			 * you could store your alignment into Alignment Server (or somewhere else) and reuse it when necessary.
			 *
			 */

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PrintWriter writer = new PrintWriter (new BufferedWriter(new OutputStreamWriter( baos, "UTF-8" )), true);

			//Printing in RDF format, but there are other formats...
			AlignmentVisitor RDFrenderer = new RDFRendererVisitor(writer);

			alignProcess.render(RDFrenderer);
			writer.flush();
			writer.close();

			System.out.println("###### Generated Alignment #######");
			System.out.println();
			System.out.println(baos.toString());

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (OntowrapException e) {
			e.printStackTrace();
		} catch (AlignmentException e) {
			e.printStackTrace();
		}

	}


}
