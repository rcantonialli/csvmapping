/**
 *
 */
package br.rcantonialli.example;

/**
 * @author Rodrigo C. Antonialli
 * 16/02/2015 - 11:43:16
 *
 */
public class Main {

	/**
	 *
	 */
	public Main() {}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		CSVtoRDFMapping mapping = new CSVtoRDFMapping();

		mapping.performMapping();

	}

}
